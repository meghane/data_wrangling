# README #

### Data Wrangling with MongoDB Udacity Coursework ###

* Tabular Formats and CSV
* Data Munging
* Web Scraping
* Data Quality

### Requirements ###

* Python 3
* MondoDB
* Dependencies coming soon
* Database configuration coming soon
* How to run tests coming soon
* Deployment instructions coming soon


### Exercises - See Top of Python Files for Specifics ###
 * Exercise 1 CSV: read csv and for each line create a dictionary, where the key is header of the field, and the value is the value.
 * Exercise 2 JSON: call web api and handle JSON response to parse specific data
 * Exercise 3 XML: extract data from xml on authors of an article

### Import primer-dataset.json into MongoDB ###
`mongoimport --db test --collection restaurants --drop --file ~/[my_dir]/primer-dataset.json`