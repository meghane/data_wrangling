from pymongo import MongoClient

client = MongoClient()
db = client.test
collection = db.restaraunts

def list_all_restaraunts_in_test_database():
    print("Listing all Restaurants . . .")

    cursor = collection.find()
    for doc in cursor:
        print(doc)

def list_manhattan_restaraunts():
    print("Listing all Restaurants in Manhattan . . .")

    cursor = collection.find({"borough": "Manhattan"})
    for doc in cursor:
        print(doc)

# Logical AND : separate conditions with comma
def get_restaraunt_by_address(building, street, zipcode):
    print("Retrieving Restaurant by the Following Address: " + building + " " + street + " " + zipcode + " . . . ")

    # nested dot-notation must be in double-quotes
    cursor = collection.find({"address.building": building, "address.zipcode": zipcode, "address.street": street})
    for doc in cursor:
        print(doc)

# Logical OR : $or query operator. Results match either condition, zipcode + borough
def get_restaraunt_by_zipcode_or_by_borough(zipcode, borough):
    print("Retrieving by Zipcode: "+ zipcode +" or by Borough: "+ borough)

    cursor = collection.find({"$or": [{"address.zipcode": zipcode}, {"borough": borough}] })
    for doc in cursor:
        print(doc)

def get_restaraunt_by_zipcode_or_by_address(building, street, zipcode, borough):
    print("Retrieving Restaurant by the Following Address: " + building + " " + street + " " + zipcode + " or by borough "
          + borough + ". . . ")

    cursor = collection.find({ "$or": [
        {"borough": borough}, {"address.building": building, "address.zipcode": zipcode, "address.street": street}
    ] })
    for doc in cursor:
        print(doc)

def test():
   building = "7"
   street = "Corson Ave"
   zipcode = "10301"
   borough = "Staten Island"

   print("Beginning Tests . . . .")
   list_all_restaraunts_in_test_database()

   print("\n . . . .\n")
   list_manhattan_restaraunts()

   print("\n . . . .\n")
   get_restaraunt_by_address(building, street, zipcode)

   print("\n . . . .\n")
   get_restaraunt_by_zipcode_or_by_borough(zipcode, borough)

   print("\n . . . .\n")
   get_restaraunt_by_zipcode_or_by_address(building, street, zipcode, borough)

test()