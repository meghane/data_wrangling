# Your task is to read the input DATAFILE and
# use Pythons CSV libs to, for each line, create a dictionary
# where the key is the header title of the field, and the value is the value of that field in the row.
# The function parse_file should return a list of dictionaries,
# each data line in the file being a single list entry.
#
import os
import csv

DATADIR = ""
DATAFILE = "beatles-discography.csv"

# https://www.python.org/dev/peps/pep-0343/
# with statement simplifies exception handling by encapsulating common preparation and cleanup tasks in so-called context managers.

def parse_file(datafile):
    data = []

    with open(datafile, "r") as csvfile:
        reader = csv.DictReader(csvfile)
        for line in reader:
            print(line)
            data.append(line)

    return data


def test():
    # a simple test of your implementation
    datafile = os.path.join(DATADIR, DATAFILE)
    d = parse_file(datafile)
    firstline = {'Title': 'Please Please Me', 'UK Chart Position': '1', 'Label': 'Parlophone(UK)',
                 'Released': '22 March 1963', 'US Chart Position': '-', 'RIAA Certification': 'Platinum',
                 'BPI Certification': 'Gold'}
    tenthline = {'Title': '', 'UK Chart Position': '1', 'Label': 'Parlophone(UK)', 'Released': '10 July 1964',
                 'US Chart Position': '-', 'RIAA Certification': '', 'BPI Certification': 'Gold'}

    assert d[0]["Title"] == 'Please Please Me'
    assert d[9]["BPI Certification"] == 'Gold'

test()
